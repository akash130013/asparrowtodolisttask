import {
  CREATE_TODO, DELETE_TODO_ITEM, HANDLE_MODAL
} from "../actions/type.js"
import { v4 as uuidv4 } from 'uuid';
// items


const initialStore = {
  todoList:
    [
    ],
  modelStatus: {
    isopen: false
  }

};

function reducer(state = initialStore, action) {

  switch (action.type) {

    case CREATE_TODO:

      let { title, description, dateTime } = action.payload;
      let newItem = { id: uuidv4(), title, description, dateTime }
      let newtodo = state.todoList.concat(newItem);

      return { ...state, todoList: newtodo }

    case DELETE_TODO_ITEM:

      let newtodoList = state.todoList.filter(item => item.id != action.payload)
      return { ...state, todoList: newtodoList }

    case HANDLE_MODAL:
      const newModelStatus = { isopen: !state.modelStatus.isopen }

      return { ...state, modelStatus: newModelStatus }

    default:
      return state;

  }

}





export default reducer