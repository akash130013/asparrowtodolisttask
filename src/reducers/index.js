import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import  ToDoReducer  from '../reducers/ToDoReducer'


export default combineReducers({
   form: formReducer,
   todo:ToDoReducer,
})