import {
    CREATE_TODO,DELETE_TODO_ITEM,HANDLE_MODAL
} from '../actions/type'

import history from '../history'


export const createTodo = formValues => async (dispatch) => {

    dispatch({
        type: CREATE_TODO,
        payload: formValues,
    })
    history.push('/');
}

export const handleTodoModal = () => async(dispatch) => {
     
    dispatch({
        type: HANDLE_MODAL,
    })
}

export const deleteToDoItem = (id) => async(dispatch) => {
  dispatch({
      type: DELETE_TODO_ITEM,
      payload: id
  })
  history.push('/');

}



