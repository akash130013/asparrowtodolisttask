import React, { Component } from 'react'
import { createTodo } from '../actions'
import { connect } from 'react-redux'
import ToDoForm from './ToDoForm'

class ToDoCreate extends Component {


    onSubmit = (formVal) => {
        this.props.createTodo(formVal);
    }

    render() {
        return (
            <>
            <h2>Create ToDo Item</h2>
            <ToDoForm  onSubmit={this.onSubmit} />
          </>
        )
    }
}

export default connect(null, { createTodo })(ToDoCreate)