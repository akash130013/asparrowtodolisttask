import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import {DateTimeInput} from 'semantic-ui-calendar-react';

class ToDoForm extends Component {

    renderError = ({ error, touched }) => {
        return error && touched && <div className="ui error message">{error}</div>
    }


    renderField = ({ input, label, meta }) => {

        return (
            <div className="field">
                <label>{label}</label>
                <input {...input} />
                <div>{this.renderError(meta)}</div>
            </div>
        )
    }

    renderDateField = ({ input, label, meta }) => {

        return (
            <DateTimeInput
           value={input.value}
           onChange={(param,data) => input.onChange(data.value)}
           placeholder={label}
           iconPosition="left"
           label={label}
             />
        )
    }

    onSubmit = (formVal) => {
        this.props.onSubmit(formVal);
    }

    render() {
        return (
            <form className="ui form error" onSubmit={this.props.handleSubmit(this.onSubmit)} >
                <Field name="title" type="text" label="Enter title" component={this.renderField} />
                <Field name="description" type="text" label="Enter description" component={this.renderField} />
                <Field name="dateTime" type="text" label="Please select data and time" component={this.renderDateField} />
                <button className="ui primary button">Submit</button>
            </form>
        )
    }
}


const validate = (formVal) => {
    const errors = {}
    if (!formVal.title) {
        errors.title = 'This field is required'
    }

    if (!formVal.description) {
        errors.description = 'This field is required'
    }


    return errors
}

export default reduxForm({
    form: 'submitToDo', // a unique identifier for this form
    validate,
})(ToDoForm)


