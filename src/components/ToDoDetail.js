import React, { useEffect, useState } from 'react'
import { deleteToDoItem,handleTodoModal } from '../actions'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Model from './Model'


function ToDoDetail({state,id,deleteToDoItem,handleTodoModal}) {
      
    
    let detail = state.todo.todoList.find(item => item.id == id)
    const isOpen=state.todo.modelStatus.isopen;
    if (!detail) {
        return <div>loading...</div>
    }
   
    const { title, description,dateTime } = detail;
    
   
    let deleteItem=(id)=>{
        deleteToDoItem(id)
        handleTodoModal();
      }
    
      let handleModal=()=>{
        handleTodoModal();
      }

    return (
        <>
            <h2>To Do Detail</h2>
            <div className="ui link cards large">

                <div className="card">

                    <div className="content">
                        <div className="header">Title: {title}</div>
                        <div className="meta">
                        <i className="large calendar alternate outline icon"></i> {dateTime ? dateTime :'N/A'}
                        </div>
                        <div className="description">
                           Description {description}
                        </div>
                        Id: {id}
                    </div>
                    <div className="extra content">
                        <Link to="/"><button type="button" className="ui button">Back</button></Link>
                       
                        <button type="button"className="ui button negative" onClick={handleModal}><i className="trash alternate outline icon"></i>Delete</button>

                    </div>
                </div>
            </div>
            <Model isOpen={isOpen} handleModal={handleModal} deleteItem={deleteItem} id={id} />

        </>
    )
}


const mapStateToProps = (state, ownProps) => {
    
    return {
        state,
        id: ownProps.match.params.id,
    }
}

export default connect(mapStateToProps, { deleteToDoItem,handleTodoModal })(ToDoDetail)