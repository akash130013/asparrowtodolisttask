import React from 'react'
import { Button, Header, Icon, Modal } from 'semantic-ui-react'

function Model({isOpen=false,handleModal,id,deleteItem}) {
   
  return (
    <Modal
      basic
      open={isOpen}
      size='small'
    >
      <Header icon>
        <Icon name='archive' />
         Delete Item!!
      </Header>
      <Modal.Content>
        <p>
       You Will never recover this.Are You Sure You want to Delete this Item?
        </p>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted onClick={handleModal}>
          <Icon name='remove' /> No
        </Button>
        <Button color='green' inverted onClick={()=>deleteItem(id)}>
          <Icon name='checkmark' /> Yes
        </Button>
      </Modal.Actions>
    </Modal>
  )
}

export default Model