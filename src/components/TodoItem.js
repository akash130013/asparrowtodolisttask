import React from 'react'
import { Link } from 'react-router-dom'
import Model from './Model'

export default function TodoItem({deleteItem, todo,handleModal,isOpen}) {

    const {id, title, description}=todo

    return (
        <>
        <div className="item">
            <i className="large hand point right outline icon"></i>
            <div className="left content">
                <Link to={`show/${id}`} className="header">{title}</Link>
                <div className="description">{description}</div>
            </div>

            <div className="right floated content">
            <Link to={`show/${id}`}><button className="ui button primary mini"><i className="eye icon"></i></button></Link>
           
            <button type="button"className="ui button negative mini" onClick={handleModal}><i className="trash alternate outline icon"></i></button>
           
            </div>
           
        </div>
        <Model isOpen={isOpen} handleModal={handleModal} deleteItem={deleteItem} id={id} />
        </>
    )
}
