import React from "react";
// components
import Navbar from "./NavBar";
import List from "./ToDoList"
import reducer from "../reducers"
import { Provider } from "react-redux"
import reduxThunk from 'redux-thunk'
import { Router, Route, Switch } from 'react-router-dom'
import history from '../history.js'
import ToDoDetail from './ToDoDetail'
import { createStore, applyMiddleware, compose } from "redux"
import ToDoCreate from "../components/ToDoCreate"

//  const store = createStore(reducer)   //used to pass reducer function
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
  applyMiddleware(reduxThunk)
));


function App() {
  // list setup

  return (
    <div className="ui container">
      <Provider store={store}>
        <Router history={history}>
          <Navbar />
         
          <Switch>
            <Route path='/' exact={true} component={List} />
            <Route path='/show/:id' component={ToDoDetail} />
            <Route path='/create'  component={ToDoCreate} />

          </Switch>
         
        </Router>

      </Provider>
    </div>
  );
}

export default App;
