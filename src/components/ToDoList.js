import React,{useState} from 'react'
import {connect} from "react-redux"
import TodoItem from './TodoItem'
import { deleteToDoItem,handleTodoModal } from '../actions'


 function TodoList({state,deleteToDoItem,handleTodoModal}) {

    const {todoList} = state.todo;
    const isOpen=state.todo.modelStatus.isopen;
  
    if(!todoList){
      return <div>Loading...</div>
    }

    if(todoList.length==0){
      return <div className="ui small message">
      No Record Found.
    </div>
    }

  let deleteItem=(id)=>{
    deleteToDoItem(id)
    handleTodoModal();
  }

  let handleModal=()=>{
    handleTodoModal();
  }

// since `todos` is an array, we can loop over it
const renderedListItems = todoList.map(todo => {   
    
  return <TodoItem key={todo.id} isOpen={isOpen} deleteItem={deleteItem} handleModal={handleModal}  todo={todo} />
})

return <ul className="ui celled list">
      
      {renderedListItems}
      
  </ul>
}


const mapStateToProps = (state)=>{
    return {state};
  }
  
  export default connect(mapStateToProps,{ deleteToDoItem,handleTodoModal}) (TodoList);
